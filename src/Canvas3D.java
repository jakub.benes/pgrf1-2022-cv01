
import model.Polygon;
import raster.*;
import render.WireRenderer;
import solids.Cube;
import solids.Solid;
import transforms.Mat4Scale;
import transforms.Mat4Transl;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.ArrayList;

public class Canvas3D {
    private final JFrame frame;
    private final JPanel panel;
    private final Raster raster;
    private final LineRasterizer lineRasterizer;

    private WireRenderer wireRenderer;
    private Solid cube, cube2;

    public Canvas3D(int width, int height)
    {
        frame = new JFrame();

        frame.setLayout(new BorderLayout());
        frame.setTitle("PGRF1");
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        raster = new RasterBufferedImage(800, 600);
        lineRasterizer = new GraphicsLineRasterizer(raster);
        wireRenderer = new WireRenderer(lineRasterizer);

        cube = new Cube();
        cube2 = new Cube();

        panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                ((RasterBufferedImage)raster).present(g);
            }
        };
        panel.setPreferredSize(new Dimension(width, height));

        frame.add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);

        panel.requestFocus();
        panel.requestFocusInWindow();
    }

    public void start() {
        raster.clear();

        List<Solid> scene = new ArrayList<>();
        scene.add(cube);
        cube.setModel(new Mat4Scale(0.5).mul(new Mat4Transl(0.2,0,0)));

        scene.add(cube2);
        Mat4Transl transl = new Mat4Transl(0.2, -0.2, 0);
        cube2.setModel(transl);
        wireRenderer.renderScene(scene);

        panel.repaint();
    }
}
