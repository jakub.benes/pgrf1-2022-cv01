package fill;

import raster.Raster;

public class SeedFiller implements Filler {

    private final int x, y;
    private final int fillColor, backgroundColor;
    private Raster raster;

    public SeedFiller(Raster raster, int x, int y, int fillColor, int backgroundColor) {
        this.raster = raster;
        this.x = x;
        this.y = y;
        this.fillColor = fillColor;
        this.backgroundColor = backgroundColor;
    }

    @Override
    public void fill() {
        seedFill(x, y);
    }

    private void seedFill(int x, int y) {
        // načíst barvu z rasteru
        int colorPixel = raster.getPixel(x, y);
        // porovnám načtenou barvu pixelu s barvou pozadí
        // když barva pixelu != barva pozadí -> končím
        if (colorPixel != backgroundColor)
            return;

        // obarvím
        raster.setPixel(x, y, fillColor);
        // pro 4 sousedy zavolám rekurzivně seedFill
        seedFill(x, y - 1);
        seedFill(x, y + 1);
        seedFill(x + 1, y);
        seedFill(x - 1, y);
    }
}
