package fill;

import model.Edge;
import model.Point;
import raster.LineRasterizer;
import raster.PolygonRasterizer;

import java.util.ArrayList;
import java.util.List;

public class ScanLineFiller implements Filler {
    private final LineRasterizer lineRasterizer;
    private final PolygonRasterizer polygonRasterizer;
    private final List<Point> points;

    public ScanLineFiller(LineRasterizer lineRasterizer, PolygonRasterizer polygonRasterizer, List<Point> points) {
        this.lineRasterizer = lineRasterizer;
        this.polygonRasterizer = polygonRasterizer;
        this.points = points;
    }

    @Override
    public void fill() {
        scanLine();
    }

    private void scanLine() {
        // vytvořit seznam hran: List<Edge>
        List<Edge> edges = new ArrayList<>();

        // Projít body a vytvořit z nich hrany
        for (int i = 0; i < points.size(); i++) {
            // Vezmu první a následující vrchol
            Point p1 = points.get(i);
            int next = (i + 1) % points.size();
            Point p2 = points.get(next);
            // Vytvořím hranu
            Edge edge = new Edge(p1.getX(), p1.getY(), p2.getX(), p2.getY());
            // Pokud je hrana horizontální, zahodím ji
            if (edge.isHorizontal())
                continue;

            // TODO: zařídit správnou orientaci hrany

            // přidám do seznamu
            edges.add(edge);
        }

        // Najít yMin, yMax
        int yMin = points.get(0).getY();
        int yMax = yMin;
        for (Point p : points) {
            // TODO: najít yMin a yMax
        }

        // Pro Y od yMin, yMax: děláme scanlines
        for (int y = yMin; y <= yMax; y++) {
            // TODO: Vytvořit seznam průsečíků List<Integer>

            // Projít všechny hrany
            // {
                // existuje průsečík?
                // pokud ano, spočítáme průsečík
                // průsečík si uložíme do listu
            // }

            // setřídění průsečíků podle x, např. BubleSort

            // spojíme pomocí line liché průsečíky se sudými
        }

        // vykrslit hranici polygonu

    }
}
