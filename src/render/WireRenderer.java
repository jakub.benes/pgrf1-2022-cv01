package render;

import model.Line;
import raster.LineRasterizer;
import solids.Solid;
import transforms.Point3D;
import transforms.Vec3D;

import java.util.List;


public class WireRenderer {

    private LineRasterizer lineRasterizer;

    public WireRenderer(LineRasterizer lineRasterizer) {
        this.lineRasterizer = lineRasterizer;
    }

    public void renderSolid(Solid solid) {
        for (int i = 0; i < solid.getIb().size(); i += 2) {
            int index1 = solid.getIb().get(i);
            int index2 = solid.getIb().get(i + 1);

            Point3D a = solid.getVb().get(index1);
            Point3D b = solid.getVb().get(index2);

            a = a.mul(solid.getModel());
            b = b.mul(solid.getModel());

            Vec3D v1 = transformToWindow(new Vec3D(a));
            Vec3D v2 = transformToWindow(new Vec3D(b));

            Line line = new Line(
                    (int) Math.round(v1.getX()), (int) Math.round(v1.getY()),
                    (int) Math.round(v2.getX()), (int) Math.round(v2.getY())
            );
            lineRasterizer.rasterize(line);
        }
    }

    private Vec3D transformToWindow(Vec3D v) {
        return v.mul(new Vec3D(1, -1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D((800 - 1) / 2., (600 - 1) / 2., 1));
    }

    public void renderScene(List<Solid> solids) {
        for (Solid solid : solids) {
            renderSolid(solid);
        }
    }
}
