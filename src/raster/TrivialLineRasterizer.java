package raster;

public class TrivialLineRasterizer extends LineRasterizer {

    public TrivialLineRasterizer(Raster raster) {
        super(raster);
    }

    @Override
    protected void drawLine(int x1, int y1, int x2, int y2) {
        // TODO: triviální alg.

        float k = (y2 - y1) / (float) (x2 - x1);
        float q = y1 - k * x1;

        // prohodit x1x2 pokud x2 < x1

        // x <= x2?? ano nebo ne?
        for (int x = x1; x <= x2; x++) {
            float y = k * x + q;
            raster.setPixel(x, Math.round(y), 0xff0000);
        }
    }
}
