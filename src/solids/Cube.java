package solids;

import transforms.Point3D;

public class Cube extends Solid {

    public Cube() {
        // Geometrii
        vb.add(new Point3D(-1, -1, 1));
        vb.add(new Point3D( 1, -1, 1));
        vb.add(new Point3D( 1,  1, 1));
        vb.add(new Point3D(-1,  1, 1));

        // Topologii
        addIndices(0, 1, 1, 2, 2, 3, 3, 0);
    }
}
